package ejemploinstanceof;
/**
 *
 * @author tinix
 */
public class EjemploInstanceOf {

    public static void main(String[] args) {
        FiguraGeometrica figura;
        figura = new Elipse();
        
        determinaTipo(figura);
        System.out.println("\nTodos sus tipos");
        determinaTodosLosTipos(figura);
    }
    
    private static void determinaTodosLosTipos(FiguraGeometrica figura){
        if (figura instanceof Elipse) {
        System.out.println("Es un Elipse");
        }
        if (figura instanceof Circulo) {
            System.out.println("Es un Circulo");
        }
        if (figura instanceof FiguraGeometrica) {
            System.out.println("Es una FiguraGeometrica");
        }
        if (figura instanceof Object) {
            System.out.println("Es un Object");
            
        } else {
        
            System.out.println("No se encontro el tipo");
        }
        
    }
    
    private static void determinaTipo(FiguraGeometrica figura){
        if (figura instanceof Elipse) {
            System.out.println("Es un Elipse");
        } else if (figura instanceof Circulo) {
            System.out.println("Es un Circulo");            
        } else if (figura instanceof FiguraGeometrica) {
            System.out.println("Es una Figura Geometrica");
        } else if (figura instanceof Object) {
            System.out.println("Es un Objeto");
        } else {
            System.out.println("No se econtro el tipo");
        }
    }
}
